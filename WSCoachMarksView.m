//
//  WSCoachMarksView.m
//  Version 0.2
//
//  Created by Dimitry Bentsionov on 4/1/13.
//  Copyright (c) 2013 Workshirt, Inc. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "WSCoachMarksView.h"

static const CGFloat kAnimationDuration = 0.1f;
static const CGFloat kCutoutRadius = 3.0f;
static const CGFloat kLblRadius = 3.0f;
static const CGFloat kMaxLblWidth = 230.0f;
static const CGFloat kLblSpacing = 35.0f;
static const CGFloat kLblPadding = 25.0f;
static const BOOL kEnableCutoutBorder = NO;
static const BOOL kEnableLblTrianglePointer = NO;
static const BOOL kEnableContinueLabel = YES;
static const BOOL kEnableSkipButton = YES;

@implementation WSCoachMarksView {
    CGRect currentRect;
    CAShapeLayer *mask;
    CAShapeLayer *cutoutMask;
    CAShapeLayer *triangleLayer;
    NSUInteger markIndex;
    UILabel *lblContinue;
    UIButton *btnSkipCoach;
}

#pragma mark - Properties

@synthesize delegate;
@synthesize coachMarks;
@synthesize lblCaption;
@synthesize cutoutBorderMaskColor = _cutoutBorderMaskColor;
@synthesize maskColor = _maskColor;
@synthesize animationDuration;
@synthesize cutoutRadius;
@synthesize lblRadius;
@synthesize maxLblWidth;
@synthesize lblSpacing;
@synthesize lblPadding;
@synthesize enableCutoutBorder;
@synthesize enableLblTrianglePointer;
@synthesize enableContinueLabel;
@synthesize enableSkipButton;

#pragma mark - Methods

- (id)initWithFrame:(CGRect)frame coachMarks:(NSArray *)marks {
    self = [super initWithFrame:frame];
    if (self) {
        // Save the coach marks
        self.coachMarks = marks;
        
        // Setup
        [self setup];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Setup
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Setup
        [self setup];
    }
    return self;
}

- (void)setup {
    // Default
    self.animationDuration = kAnimationDuration;
    self.cutoutRadius = kCutoutRadius;
    self.lblRadius = kLblRadius;
    self.maxLblWidth = kMaxLblWidth;
    self.lblSpacing = kLblSpacing;
    self.lblPadding = kLblPadding;
    self.enableCutoutBorder = kEnableCutoutBorder;
    self.enableLblTrianglePointer = kEnableLblTrianglePointer;
    self.enableContinueLabel = kEnableContinueLabel;
    self.enableSkipButton = kEnableSkipButton;
    
    // Shape layer mask
    mask = [CAShapeLayer layer];
    [mask setFillRule:kCAFillRuleEvenOdd];
    [mask setFillColor:[[UIColor colorWithHue:0.0f saturation:0.0f brightness:0.0f alpha:0.75f] CGColor]];
    [self.layer addSublayer:mask];
    
    // cutoutMask layer mask
    cutoutMask = [CAShapeLayer layer];
    [cutoutMask setFillRule:kCAFillRuleEvenOdd];
    [cutoutMask setFillColor:[[UIColor colorWithRed:18/255.0 green:172/255.0 blue:230/255.0 alpha:1] CGColor]];
    [self.layer addSublayer:cutoutMask];
    
    triangleLayer = [CAShapeLayer layer];
    [triangleLayer setFillColor:[[UIColor colorWithRed:18/255.0 green:172/255.0 blue:230/255.0 alpha:1] CGColor]];
    [self.layer addSublayer:triangleLayer];
    
    // Capture touches
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userDidTap:)];
    [self addGestureRecognizer:tapGestureRecognizer];
    
    // Captions
    self.lblCaption = [[UILabel alloc] initWithFrame:(CGRect){{0.0f, 0.0f}, {self.maxLblWidth, 0.0f}}];
    self.lblCaption.backgroundColor = [UIColor clearColor];
    self.lblCaption.textColor = [UIColor whiteColor];
    self.lblCaption.font = [UIFont systemFontOfSize:20.0f];
    self.lblCaption.lineBreakMode = NSLineBreakByWordWrapping;
    self.lblCaption.numberOfLines = 0;
    self.lblCaption.textAlignment = NSTextAlignmentCenter;
    self.lblCaption.alpha = 0.0f;
    self.lblCaption.layer.masksToBounds = YES;
    self.lblCaption.layer.cornerRadius = self.lblRadius;
    [self addSubview:self.lblCaption];
    
    // Hide until unvoked
    self.hidden = YES;
}

#pragma mark - Cutout modify

- (void)setCutoutToRect:(CGRect)rect {
    // Define shape

    if(enableLblTrianglePointer){
        [triangleLayer setPath:[self getLblCaptionTrianglePath:rect].CGPath];
    }
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRect:self.bounds];
    
    if(enableCutoutBorder) {
        int cutoutPadding = 3;
        UIBezierPath *cutoutBorderPath = [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:self.cutoutRadius];
        UIBezierPath *cutoutPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(rect.origin.x + cutoutPadding, rect.origin.y + cutoutPadding, rect.size.width - (2*cutoutPadding), rect.size.height - (2*cutoutPadding)) cornerRadius:self.cutoutRadius];
        
        [maskPath appendPath:cutoutBorderPath];
        [cutoutBorderPath appendPath:cutoutPath];
        
        mask.path = maskPath.CGPath;
        cutoutMask.path = cutoutBorderPath.CGPath;
        
    } else {
        UIBezierPath *cutoutPath = [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:self.cutoutRadius];
        [maskPath appendPath:cutoutPath];
        mask.path = maskPath.CGPath;
    }
}

- (void)animateCutoutToRect:(CGRect)rect {

    if(enableLblTrianglePointer){
        [triangleLayer setPath:[self getLblCaptionTrianglePath:rect].CGPath];
    }
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRect:self.bounds];
    
    if(enableCutoutBorder) {
        
        int cutoutPadding = 3;
        UIBezierPath *cutoutBorderPath = [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:self.cutoutRadius];
        UIBezierPath *cutoutPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(rect.origin.x + cutoutPadding, rect.origin.y + cutoutPadding, rect.size.width - (2*cutoutPadding), rect.size.height - (2*cutoutPadding)) cornerRadius:self.cutoutRadius];
        
        [maskPath appendPath:cutoutBorderPath];
        [cutoutBorderPath appendPath:cutoutPath];
        
        mask.path = maskPath.CGPath;
        cutoutMask.path = cutoutBorderPath.CGPath;
        
    } else {
        UIBezierPath *cutoutPath = [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:self.cutoutRadius];
        
        [maskPath appendPath:cutoutPath];
        
        mask.path = maskPath.CGPath;
    }
    
    // Animate it
    CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"path"];
    anim.delegate = self;
    anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    anim.duration = self.animationDuration;
    anim.removedOnCompletion = NO;
    anim.fillMode = kCAFillModeForwards;
    anim.fromValue = (__bridge id)(mask.path);
    anim.toValue = (__bridge id)(maskPath.CGPath);
    [mask addAnimation:anim forKey:@"path"];
    mask.path = maskPath.CGPath;
}

#pragma mark - Mask color

- (void)setMaskColor:(UIColor *)maskColor {
    _maskColor = maskColor;
    [mask setFillColor:[maskColor CGColor]];
}

- (void)setCutoutBorderMaskColor:(UIColor *)cutoutBorderMaskColor {
    _cutoutBorderMaskColor = cutoutBorderMaskColor;
    [cutoutMask setFillColor:[cutoutBorderMaskColor CGColor]];
}

#pragma mark - Touch handler

- (void)userDidTap:(UITapGestureRecognizer *)recognizer {
    // Go to the next coach mark
    
    CGPoint p = [recognizer locationInView:self];
    if (CGRectContainsPoint(currentRect, p)) {
        [self goToCoachMarkIndexed:(markIndex+1)];
    } else {
    }
}

#pragma mark - Navigation

- (void)start {
    // Fade in self
    self.alpha = 0.0f;
    self.hidden = NO;
    [UIView animateWithDuration:self.animationDuration
                     animations:^{
                         self.alpha = 1.0f;
                     }
                     completion:^(BOOL finished) {
                         // Go to the first coach mark
                         [self goToCoachMarkIndexed:0];
                     }];
}

- (void)skipCoach {
    // Delegate (coachMarksViewDidEnd:)
    if ([self.delegate respondsToSelector:@selector(coachMarksViewDidEnd:)]) {
        [self.delegate coachMarksViewDidEnd:self];
    }
    [self goToCoachMarkIndexed:self.coachMarks.count];
}

- (void)goToCoachMarkIndexed:(NSUInteger)index {
    // Out of bounds
    if (index >= self.coachMarks.count) {
        [self cleanup];
        // Delegate (coachMarksViewDidEnd:)
        if ([self.delegate respondsToSelector:@selector(coachMarksViewDidEnd:)]) {
            [self.delegate coachMarksViewDidEnd:self];
        }
        return;
    }
    
    // Current index
    markIndex = index;
    
    // Coach mark definition
    NSDictionary *markDef = [self.coachMarks objectAtIndex:index];
    NSString *markCaption = [markDef objectForKey:@"caption"];
    CGRect markRect = [[markDef objectForKey:@"rect"] CGRectValue];
    currentRect = markRect;
    
    // Delegate (coachMarksView:willNavigateTo:atIndex:)
    if ([self.delegate respondsToSelector:@selector(coachMarksView:willNavigateToIndex:)]) {
        [self.delegate coachMarksView:self willNavigateToIndex:markIndex];
    }
    
    // Calculate the caption position and size
    self.lblCaption.alpha = 0.0f;
    self.lblCaption.frame = (CGRect){{0.0f, 0.0f}, {self.maxLblWidth, 0.0f}};
    self.lblCaption.text = markCaption;
    [self.lblCaption sizeToFit];
    CGFloat y = markRect.origin.y + markRect.size.height + self.lblSpacing;
    CGFloat bottomY = y + self.lblCaption.frame.size.height + self.lblSpacing;
    if (bottomY > self.bounds.size.height) {
        y = markRect.origin.y - self.lblSpacing - self.lblCaption.frame.size.height;
    }
    CGFloat x = floorf((self.bounds.size.width - self.lblCaption.frame.size.width) / 2.0f);
    
    x = x  - self.lblPadding;
    y = y  - (self.lblPadding/2);
    
    // Animate the caption label
    self.lblCaption.frame = (CGRect){{x, y}, {self.lblCaption.frame.size.width + (self.lblPadding * 2), self.lblCaption.frame.size.height + (self.lblPadding)}};
    [UIView animateWithDuration:self.animationDuration animations:^{
        self.lblCaption.alpha = 1.0f;
    }];
    
    // If first mark, set the cutout to the center of first mark
    if (markIndex == 0) {
        CGPoint center = CGPointMake(floorf(markRect.origin.x + (markRect.size.width / 2.0f)), floorf(markRect.origin.y + (markRect.size.height / 2.0f)));
        CGRect centerZero = (CGRect){center, CGSizeZero};
        [self setCutoutToRect:centerZero];
    }
    
    // Change fill color of label triangle
    [triangleLayer setFillColor:[[self.lblCaption backgroundColor] CGColor]];
    
    // Animate the cutout
    [self animateCutoutToRect:markRect];
    
    CGFloat lblContinueWidth = self.enableSkipButton ? (70.0/100.0) * self.bounds.size.width : self.bounds.size.width;
    CGFloat btnSkipWidth = self.bounds.size.width - lblContinueWidth;
    
    // Show continue lbl if first mark
    if (self.enableContinueLabel) {
        if (markIndex == 0) {
            lblContinue = [[UILabel alloc] initWithFrame:(CGRect){{0, self.bounds.size.height - 30.0f}, {lblContinueWidth, 30.0f}}];
            lblContinue.font = [UIFont boldSystemFontOfSize:13.0f];
            lblContinue.textAlignment = NSTextAlignmentCenter;
            lblContinue.text = @"Tap to continue";
            lblContinue.alpha = 0.0f;
            lblContinue.backgroundColor = [UIColor whiteColor];
            [self addSubview:lblContinue];
            [UIView animateWithDuration:0.3f delay:1.0f options:0 animations:^{
                lblContinue.alpha = 1.0f;
            } completion:nil];
        } else if (markIndex > 0 && lblContinue != nil) {
            // Otherwise, remove the lbl
            [lblContinue removeFromSuperview];
            lblContinue = nil;
        }
    }
    
    if (self.enableSkipButton) {
        btnSkipCoach = [[UIButton alloc] initWithFrame:(CGRect){{lblContinueWidth, self.bounds.size.height - 30.0f}, {btnSkipWidth, 30.0f}}];
        [btnSkipCoach addTarget:self action:@selector(skipCoach) forControlEvents:UIControlEventTouchUpInside];
        [btnSkipCoach setTitle:@"Skip" forState:UIControlStateNormal];
        btnSkipCoach.titleLabel.font = [UIFont boldSystemFontOfSize:13.0f];
        btnSkipCoach.alpha = 0.0f;
        btnSkipCoach.tintColor = [UIColor whiteColor];
        [self addSubview:btnSkipCoach];
        [UIView animateWithDuration:0.3f delay:1.0f options:0 animations:^{
            btnSkipCoach.alpha = 1.0f;
        } completion:nil];
    }
}

#pragma mark - Cleanup

- (void)cleanup {
    // Delegate (coachMarksViewWillCleanup:)
    if ([self.delegate respondsToSelector:@selector(coachMarksViewWillCleanup:)]) {
        [self.delegate coachMarksViewWillCleanup:self];
    }
    
    // Fade out self
    [UIView animateWithDuration:self.animationDuration
                     animations:^{
                         self.alpha = 0.0f;
                     }
                     completion:^(BOOL finished) {
                         // Remove self
                         [self removeFromSuperview];
                         
                         // Delegate (coachMarksViewDidCleanup:)
                         if ([self.delegate respondsToSelector:@selector(coachMarksViewDidCleanup:)]) {
                             [self.delegate coachMarksViewDidCleanup:self];
                         }
                     }];
}

#pragma mark - Animation delegate

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
    // Delegate (coachMarksView:didNavigateTo:atIndex:)
    if ([self.delegate respondsToSelector:@selector(coachMarksView:didNavigateToIndex:)]) {
        [self.delegate coachMarksView:self didNavigateToIndex:markIndex];
    }
}

#pragma mark - Label triangle pointer
// http://stackoverflow.com/a/28422028
- (UIBezierPath *)getLblCaptionTrianglePath:(CGRect)rect
{
    float width = 20;
    
    CGPoint startPoint;
    
    if(rect.origin.y >= (([[UIScreen mainScreen] bounds].size.height) * 0.50)) {
        startPoint = CGPointMake((CGRectGetMidX(rect)-(width/2)), CGRectGetMaxY(self.lblCaption.frame));
    } else {
        startPoint = CGPointMake((CGRectGetMidX(rect)-(width/2)), self.lblCaption.frame.origin.y);
    }
    
    if(startPoint.x - width < self.lblCaption.frame.origin.x){
        startPoint.x = self.lblCaption.frame.origin.x + width;
    }
    
    CGPoint peakPoint, closePoint;
    
    if(rect.origin.y >= (([[UIScreen mainScreen] bounds].size.height) * 0.50)) {
        peakPoint = CGPointMake((startPoint.x + (width/2)), CGRectGetMinY(rect) - (width/2));
        closePoint = CGPointMake((startPoint.x + width), CGRectGetMaxY(self.lblCaption.frame));
    } else {
        peakPoint = CGPointMake((startPoint.x + (width/2)), CGRectGetMaxY(rect) + (width/2));
        closePoint = CGPointMake((startPoint.x + width), self.lblCaption.frame.origin.y);
    }
    
    UIBezierPath *trianglePath = [UIBezierPath bezierPath];
    
    [trianglePath moveToPoint:startPoint];
    [trianglePath addLineToPoint:peakPoint];
    [trianglePath addLineToPoint:closePoint];
    [trianglePath closePath];
    
    return trianglePath;
}

@end
